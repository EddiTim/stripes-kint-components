import { forwardRef, useImperativeHandle } from 'react';

import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { useInfiniteQuery } from 'react-query';

import {
  useNamespace,
  useOkapiKy
} from '@folio/stripes/core';

import {
  CollapseFilterPaneButton,
  ExpandFilterPaneButton,
  SearchAndSortQuery,
  PersistedPaneset,
} from '@folio/stripes/smart-components';

import {
  Button,
  Icon,
  Pane,
  PaneMenu,
  SearchField,
} from '@folio/stripes/components';

import { generateKiwtQuery } from '../utils';
import { useKintIntl, useKiwtSASQuery, useLocalStorageState } from '../hooks';

import TableBody from './TableBody';

const SASQLookupComponent = forwardRef((props, ref) => {
  const {
    children,
    fetchParameters = {},
    FilterComponent = () => null,
    FilterPaneHeaderComponent = () => null,
    filterPaneProps = {},
    id,
    intlKey: passedIntlKey,
    intlNS: passedIntlNS,
    labelOverrides = {},
    mainPaneProps,
    noSearchField,
    persistedPanesetProps = {},
    RenderBody,
    sasqProps,
    searchFieldAriaLabel,
    searchFieldProps
  } = props;
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const { query, queryGetter, querySetter } = useKiwtSASQuery();
  const { 0: namespace } = useNamespace();
  const ky = useOkapiKy();

  const filterPaneVisibileKey = `${namespace}-${id}-filterPaneVisibility`;

  const fetchPageData = ({ pageParam = 0 }) => {
    const queryMap = fetchParameters.SASQ_MAP;
    queryMap.offset = pageParam;
    return ky(`${fetchParameters.endpoint}${generateKiwtQuery(queryMap, query)}`).json();
  };

  const [filterPaneVisible, setFilterPaneVisible] = useLocalStorageState(filterPaneVisibileKey, true);
  const toggleFilterPane = () => setFilterPaneVisible(!filterPaneVisible);

  const queryNamespace = [namespace, 'SASQ'];
  if (id) {
    queryNamespace.push(id);
  }
  queryNamespace.push('viewAll');
  queryNamespace.push(query);

  const {
    data: totalData = {},
    ...restOfInfiniteQueryProps
  } = useInfiniteQuery(
    queryNamespace,
    fetchPageData
  );

  useImperativeHandle(ref, () => (
    {
      lookupQueryProps: {
        data: totalData,
        ...restOfInfiniteQueryProps
      }
    }
  ));

  const data = totalData.pages?.reduce(
    (acc, curr) => {
      const newAcc = { ...acc };
      for (const [key, value] of Object.entries(curr)) {
        if (
          key !== 'page' &&
          key !== 'result' &&
          acc[key] !== value
        ) {
          newAcc[key] = value;
        }
      }

      const newResults = [...(acc.results ?? [])];
      newResults.push(...(curr.results ?? []));
      newAcc.results = newResults;

      return newAcc;
    },
    {}
  ) ?? {};

  return (
    <SearchAndSortQuery
      initialSearchState={{ query: '' }}
      queryGetter={queryGetter}
      querySetter={querySetter}
      {...sasqProps}
    >
      {
        (sasqRenderProps) => {
          const {
          activeFilters,
          filterChanged,
          getFilterHandlers,
          getSearchHandlers,
          onSubmitSearch,
          resetAll,
          searchChanged,
          searchValue
        } = sasqRenderProps;

          const searchHandlers = getSearchHandlers();
          const disableReset = !filterChanged && !searchChanged;

          const filterCount = activeFilters.string ? activeFilters.string.split(',').length : 0;

          const Body = RenderBody ?? TableBody;

          const {
            filterPaneFirstMenu,
            filterPaneLastMenu,
            ...restOfFilterPaneProps
            } = filterPaneProps;
          const {
            mainPaneFirstMenu,
            mainPaneLastMenu,
            ...restOfMainPaneProps
          } = mainPaneProps;

          const internalStateProps = {
            activeFilters,
            filterCount,
            filterPaneVisible,
            searchChanged,
            searchValue,
            setFilterPaneVisible,
            toggleFilterPane
          };

          return (
            <PersistedPaneset
              appId={namespace}
              id={`${id}-paneset`}
              {...persistedPanesetProps}
            >
              {filterPaneVisible &&
                <Pane
                  defaultWidth="20%"
                  firstMenu={filterPaneFirstMenu ?
                    filterPaneFirstMenu(internalStateProps) :
                    null
                  }
                  id={`${id}-filter-pane`}
                  lastMenu={filterPaneLastMenu ?
                    filterPaneLastMenu(internalStateProps) :
                    <PaneMenu>
                      <CollapseFilterPaneButton onClick={toggleFilterPane} />
                    </PaneMenu>
                  }
                  paneTitle={<FormattedMessage id="stripes-smart-components.searchAndFilter" />}
                  {...restOfFilterPaneProps}
                >
                  <form onSubmit={onSubmitSearch}>
                    <FilterPaneHeaderComponent />
                    {!noSearchField &&
                      <>
                        <SearchField
                          ariaLabel={searchFieldAriaLabel}
                          autoFocus
                          id={`sasq-search-field-${id}`}
                          name="query"
                          onChange={e => {
                            if (e.target?.value) {
                              searchHandlers.query(e); // SASQ needs the whole event here
                            } else {
                              searchHandlers.reset();
                            }
                          }}
                          onClear={searchHandlers.reset}
                          value={searchValue.query}
                          {...searchFieldProps}
                        />
                        <Button
                          buttonStyle="primary"
                          disabled={!searchValue.query}
                          fullWidth
                          type="submit"
                        >
                          <FormattedMessage id="stripes-smart-components.search" />
                        </Button>
                        <Button
                          buttonStyle="none"
                          disabled={disableReset}
                          id="clickable-reset-all"
                          onClick={resetAll}
                        >
                          <Icon icon="times-circle-solid">
                            <FormattedMessage id="stripes-smart-components.resetAll" />
                          </Icon>
                        </Button>
                      </>
                    }
                    <FilterComponent
                      activeFilters={activeFilters.state}
                      filterChanged={filterChanged}
                      filterHandlers={getFilterHandlers()}
                      resetAll={resetAll}
                      searchChanged={searchChanged}
                      searchHandlers={getSearchHandlers()}
                      searchValue={searchValue}
                    />
                  </form>
                </Pane>
              }
              <Pane
                defaultWidth="fill"
                firstMenu={mainPaneFirstMenu ?
                  mainPaneFirstMenu(internalStateProps) :
                  !filterPaneVisible ?
                    <PaneMenu>
                      <ExpandFilterPaneButton filterCount={filterCount} onClick={toggleFilterPane} />
                    </PaneMenu> :
                    null
                }
                id={`${id}-main-pane`}
                lastMenu={mainPaneLastMenu ?
                  mainPaneLastMenu(internalStateProps) :
                  null
                }
                noOverflow
                padContent={false}
                paneSub={
                  kintIntl.formatKintMessage({
                    id: 'found#Values',
                    overrideValue: labelOverrides?.foundValues
                  }, { total: data?.total })
                }
                {...restOfMainPaneProps}
              >
                <Body
                  data={data}
                  filterPaneVisible={filterPaneVisible}
                  intlKey={passedIntlKey}
                  intlNS={passedIntlNS}
                  labelOverrides={labelOverrides}
                  query={query}
                  toggleFilterPane={toggleFilterPane}
                  {...restOfInfiniteQueryProps}
                  {...sasqRenderProps}
                  /*
                   * This is insane, it looks like SASQProps `initialSortState`
                   * needs to be passed through to MCL, as it relies on MCL calling the initial
                   * sort handler. Passing through SASQProps.
                   */
                  {...sasqProps}
                  {...props}
                />
              </Pane>
              {children}
            </PersistedPaneset>
          );
        }
      }
    </SearchAndSortQuery>
  );
});

SASQLookupComponent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  fetchParameters: PropTypes.object,
  filterPaneProps: PropTypes.object,
  FilterComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  FilterPaneHeaderComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  history: PropTypes.object,
  id: PropTypes.string.isRequired,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  location: PropTypes.object,
  mainPaneProps: PropTypes.object,
  match: PropTypes.object,
  mclProps: PropTypes.object,
  noSearchField: PropTypes.bool,
  path: PropTypes.string.isRequired,
  persistedPanesetProps: PropTypes.object,
  RenderBody: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
  resource: PropTypes.object,
  resultColumns: PropTypes.arrayOf(PropTypes.object),
  sasqProps: PropTypes.object,
  searchFieldAriaLabel: PropTypes.string,
  searchFieldProps: PropTypes.object
};

export default SASQLookupComponent;
