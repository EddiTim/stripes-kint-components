# util
A collection of useful util functions

## generateKiwtQuery
A util function for generating a "KIWT" (K-Int Web-Toolkit) style backend query from a SASQ query object
### Basic usage
```
import { generateKiwtQuery } from '@k-int/stripes-kint-components'
...
  const nsValues = {
    query: 'test',
    filters: 'requestStatus.completed,journalVolume.test1,startDate.startDate>=2021-10-28'
  }

  const options = {
    searchKey: 'request.name',
    filterKeys: {
      requestStatus: 'requestStatus.value,
      journalVolume: 'journalVolume'
    }
  }

  const queryString = generateKiwtQuery(options, nsValues)

  // We'd expect queryString === "?match=request.name&term=test&filters=requestStatus.value==completed&filters=journalVolume==test1&filters=startDate%3E=2021-10-28&stats=true"
...
```

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
options | object | An object with keys: `searchKey`, `filterKeys`, `sortKeys` and `stats`, which maps the incoming nsValues objects to a KIWT query. You can also pass arbitrary `key`/`value` pairs to append `key==value` onto the query. | | ✓ |
nsValues | object | An object containing the actual query parameters to become the mapped KIWT query | | ✓ |
encode | boolean | A boolean prop which determines if each query param chunk will be encoded using encodeURIComponent or not | true | ✕ |

## generateKiwtQueryParams
A util function for generating an array of "KIWT" (K-Int Web-Toolkit) style backend query parameters from a SASQ query object
### Basic usage
```
import { generateKiwtQueryParams } from '@k-int/stripes-kint-components'
...
  const nsValues = {
    query: 'test',
    filters: 'requestStatus.completed,journalVolume.test1,startDate.startDate>=2021-10-28'
  }

  const options = {
    searchKey: 'request.name',
    filterKeys: {
      requestStatus: 'requestStatus.value,
      journalVolume: 'journalVolume'
    }
  }

  const queryArray = generateKiwtQueryParams(options, nsValues)

  // We'd expect queryArray === [
    "match=request.name",
    "term=test",
    "filters=requestStatus.value==completed",
    "filters=journalVolume==test1",
    "filters=startDate%3E=2021-10-28",
    "stats=true"
  ]
...
```

### Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
options | object | An object with keys: `searchKey`, `filterKeys`, `sortKeys` and `stats`, which maps the incoming nsValues objects to an KIWT query array. You can also pass arbitrary `key`/`value` pairs to append `key==value` onto the query. | | ✓ |
nsValues | object | An object containing the actual query parameters to become the mapped KIWT query | | ✓ |
encode | boolean | A boolean prop which determines if each query param chunk will be encoded using encodeURIComponent or not | true | ✕ |