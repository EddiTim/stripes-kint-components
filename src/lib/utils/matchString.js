import escapeRegExp from 'lodash/escapeRegExp';

const matchString = (match, str, ignoreNull = true) => {
  const regex = new RegExp(`${match.split(/(\s+)/).filter(h => h.trim()).map(hl => '(' + escapeRegExp(hl) + ')').join('|')}`, 'gi');
  if (ignoreNull && !match) {
    const nullRegex = /a^/gi; // Should match nothing

    return [[str], nullRegex];
  }

  return [str.split(regex), regex];
};

export default matchString;
