const toCamelCase = (str) => (
  // First split string on any capital letters
  str.split(/(?=[A-Z])/).join(' ')
  // Remove all non alpha-numeric chars, and replace _ with ' '
    .replace(/[_]/gi, ' ')
    .replace(/[^0-9a-zA-Z\s]/gi, '')
  // CamelCase the remains
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) => (idx === 0 ?
      ltr.toLowerCase() :
      ltr.toUpperCase()))
    .replace(/\s+/g, '')
);

export default toCamelCase;
