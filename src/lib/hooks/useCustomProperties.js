import PropTypes from 'prop-types';

import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';

import { generateKiwtQuery } from '../utils';

const useCustomProperties = ({
  endpoint,
  ctx,
  nsValues = {
    sort: 'id'
  },
  options = {},
  queryParams,
  returnQueryObject = false,
}) => {
  const ky = useOkapiKy();

  const custPropOptions = {
    searchKey: 'label,name,description',
    filterKeys: {
      ContextKey: 'ctx'
    },
    sort: [
      { path: 'weight' },
      { path: 'label' }
    ],
    filters: [],
    stats: false,
    max: 100,
    ...options
  };

  if (Array.isArray(ctx)) {
    // If we have an array, append a context filter for each ctx given

    // Special case if one is isNull
    custPropOptions.filters.push({
      values: ctx.map(c => (c === 'isNull' ? 'ctx isNull' : `ctx==${c}`))
    });
  } else if (ctx === 'isNull') { // isNull is a special case
    custPropOptions.filters.push({
      value: 'ctx isNull'
    });
  } else if (ctx) {
    custPropOptions.filters.push({
      path: 'ctx',
      value: ctx
    });
  }

  const query = generateKiwtQuery(custPropOptions, nsValues);
  const path = `${endpoint}${query}`;

  const queryObject = useQuery(
    ['stripes-kint-components', 'useCustomProperties', 'custprops', ctx, path],
    () => ky(path).json(),
    queryParams
  );

  if (returnQueryObject) {
    return queryObject || {};
  }

  const { data: custprops } = queryObject;
  return custprops || [];
};

useCustomProperties.propTypes = {
  endpoint: PropTypes.string,
  ctx: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  queryParams: PropTypes.object,
  returnQueryObject: PropTypes.bool
};

export default useCustomProperties;
