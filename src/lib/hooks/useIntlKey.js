import useIntlKeyStore from './useIntlKeyStore';

const useIntlKey = (passedIntlKey, passedIntlNS) => {
  const getKey = useIntlKeyStore(state => state.getKey);
  let intlKey;

  if (passedIntlKey) {
    intlKey = passedIntlKey;
  } else if (passedIntlNS) {
    intlKey = getKey(passedIntlNS);
  } else {
    intlKey = getKey();
  }

  if (!intlKey) {
    throw new Error('Stripes kint components requires an intl key to be either configured via `addKey` from `useIntlKeyStore` or passed directly into components');
  }

  return intlKey;
};

export default useIntlKey;
