import PropTypes from 'prop-types';

import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { modConfigEntriesQueryKey, parseModConfigEntry } from '../utils';
import { MOD_SETTINGS_ENDPOINT } from '../constants/endpoints';

const useModConfigEntries = ({
  configName,
  moduleName,
  namespaceAppend,
  queryParams
}) => {
  const ky = useOkapiKy();

  const query = `?query=(module=${moduleName} and configName=${configName})`;
  const path = `${MOD_SETTINGS_ENDPOINT}${query}`;

  const namespace = modConfigEntriesQueryKey({
    configName,
    moduleName,
    namespaceAppend
  });

  const queryObject = useQuery(
    namespace,
    () => ky(path).json(),
    queryParams
  );

  const { data: { configs: { 0: settings = {} } = [] } = {} } = queryObject;
  const parsedSettings = parseModConfigEntry(settings);

  return {
    parsedSettings,
    queryObject,
    settings
  };
};

useModConfigEntries.propTypes = {
  moduleName: PropTypes.string,
  configName: PropTypes.string,
  queryParams: PropTypes.object,
};

export default useModConfigEntries;
