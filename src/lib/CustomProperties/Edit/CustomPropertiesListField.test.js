import React from 'react';

import { Button, KeyValue, TestForm } from '@folio/stripes-erm-testing';
import { waitFor } from '@folio/jest-config-stripes/testing-library/react';

import { initialValues } from './testResources';
import CustomPropertiesListField from './CustomPropertiesListField';
import { customProperties, renderWithKintHarness } from '../../../../test/jest';

const onSubmit = jest.fn();
jest.mock('../../hooks');
jest.mock('./CustomPropertyFormCard', () => () => <div>CustomPropertyFormCard</div>);

describe('CustomPropertiesListField', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <TestForm
        initialValues={initialValues}
        onSubmit={onSubmit}
      >
        <CustomPropertiesListField
          ctx="isNull"
          customProperties={customProperties}
          name="customProperties"
        />
      </TestForm>
    );
  });

  test('displays expected primary properties key value', async () => {
    await KeyValue('Primary properties').exists();
  });

  test('displays expected optional properties key value', async () => {
    await KeyValue('Optional properties').exists();
  });

  it('renders CustomPropertyFormCard component ', () => {
    const { getAllByText } = renderComponent;
    waitFor(() => expect(getAllByText('CustomPropertyFormCard')).toBeInTheDocument());
  });

  it('renders CustomPropertiesList component ', () => {
    const { getAllByText } = renderComponent;
    waitFor(() => expect(getAllByText('CustomPropertiesList')).toBeInTheDocument());
  });

  test('renders the Add property button', async () => {
    await Button('Add property').exists();
  });
});
