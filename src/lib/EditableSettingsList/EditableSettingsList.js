import React from 'react';
import PropTypes from 'prop-types';
import { FieldArray } from 'react-final-form-arrays';
import { Form } from 'react-final-form';
import arrayMutators from 'final-form-arrays';

import EditableSettingsListFieldArray from './EditableSettingsListFieldArray';

const EditableSettingsList = ({
  allowEdit = true, // A gloabal boolean permission to turn on/off editing of all appSettings in the frontend
  data,
  initialValues,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  label,
  labelOverrides = {},
  onSave
}) => {
  return (
    <Form
      enableReinitialize
      initialValues={initialValues}
      keepDirtyOnReinitialize
      mutators={arrayMutators}
      navigationCheck
      onSubmit={onSave}
      subscription={{ value: true }}
    >
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <FieldArray
            allowEdit={allowEdit}
            component={EditableSettingsListFieldArray}
            data={data}
            intlKey={passedIntlKey}
            intlNS={passedIntlNS}
            label={label}
            labelOverrides={labelOverrides}
            name="settings"
            onSave={onSave}
          />
        </form>
      )}
    </Form>
  );
};

EditableSettingsList.propTypes = {
  allowEdit: PropTypes.bool,
  data: PropTypes.shape({
    refdatavalues: PropTypes.arrayOf(PropTypes.object)
  }),
  initialValues: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.object
  ]),
  labelOverrides: PropTypes.object,
  onSave: PropTypes.func,
};

export default EditableSettingsList;
